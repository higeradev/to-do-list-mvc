package taskmanager.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import taskmanager.models.Task;
import taskmanager.services.TaskService;

import java.util.List;

@Controller
public class FilterTaskController {
    private final TaskService service;

    public FilterTaskController(TaskService service) {
        this.service = service;
    }

    @GetMapping("/filter/title")
    public String filterByWordInTitle(@RequestParam String title, Model model) {
        List<Task> tasks = service.filterByWordInTitle(title);

        model.addAttribute("tasks", service.getTaskAttributesDTO(tasks));
        return "tasks";
    }

    @GetMapping("/filter/description")
    public String filterByWordInDescription(@RequestParam String description, Model model) {
        List<Task> tasks = service.filterByWordInDescription(description);

        model.addAttribute("tasks", service.getTaskAttributesDTO(tasks));
        return "tasks";
    }

    @GetMapping("/filter/priority")
    public String filterByPriority(@RequestParam String priority, Model model) {
        List<Task> tasks = service.filterByPriority(priority);

        model.addAttribute("tasks", service.getTaskAttributesDTO(tasks));
        return "tasks";
    }

    @GetMapping("/filter/state")
    public String filterByState(@RequestParam String state, Model model) {
        List<Task> tasks = service.filterByState(state);

        model.addAttribute("tasks", service.getTaskAttributesDTO(tasks));
        return "tasks";
    }

}
