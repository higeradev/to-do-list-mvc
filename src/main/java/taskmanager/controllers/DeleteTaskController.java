package taskmanager.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import taskmanager.services.TaskService;

@Controller
public class DeleteTaskController {
    private final TaskService service;

    public DeleteTaskController(TaskService service) {
        this.service = service;
    }

    @PostMapping("/delete/task/{id}")
    public String delete(@PathVariable String id) {
        service.delete(id);

        return "redirect:/";
    }
}
