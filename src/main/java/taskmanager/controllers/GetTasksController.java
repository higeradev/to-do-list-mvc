package taskmanager.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import taskmanager.models.Priority;
import taskmanager.models.States;
import taskmanager.models.Task;
import taskmanager.services.TaskService;

import java.util.List;

@Controller
public class GetTasksController {
    private final TaskService service;

    public GetTasksController(TaskService service) {
        this.service = service;
    }

    @GetMapping("/")
    public String get(Model model) {
        List<Task> tasks = service.findAll();

        model.addAttribute("tasks", service.getTaskAttributesDTO(tasks));

        return "tasks";
    }
}
