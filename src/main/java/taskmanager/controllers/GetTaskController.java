package taskmanager.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import taskmanager.models.Task;
import taskmanager.services.TaskService;

@Controller
public class GetTaskController {
    private final TaskService service;

    public GetTaskController(TaskService service) {
        this.service = service;
    }

    @GetMapping("/tasks/{id}/{count}")
    public String get(@PathVariable String id, @PathVariable String count, Model model) {
        Task task = service.findById(id);

        model.addAttribute("task", task);
        model.addAttribute("count", count);

        return "task";
    }
}
