package taskmanager.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import taskmanager.models.Priority;
import taskmanager.models.States;
import taskmanager.models.Task;
import taskmanager.services.TaskService;
import taskmanager.utils.Generator;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

@Controller
public class SortTaskController {
    private final TaskService service;

    public SortTaskController(TaskService service) {
        this.service = service;
    }

    @GetMapping("/sort/title/asc")
    public String orderByTitleAsc(Model model) {
        List<Task> tasks = service.orderByTitleAsc();

        model.addAttribute("tasks", service.getTaskAttributesDTO(tasks));
        return "tasks";
    }

    @GetMapping("/sort/title/desc")
    public String orderByTitleDesc(Model model) {
        List<Task> tasks = service.orderByTitleDesc();

        model.addAttribute("tasks", service.getTaskAttributesDTO(tasks));

        return "tasks";
    }

    @GetMapping("/sort/priority/asc")
    public String orderByPriorityAsc(Model model) {
        List<Task> tasks = service.orderByPriorityAsc();

        model.addAttribute("tasks", service.getTaskAttributesDTO(tasks));

        return "tasks";
    }

    @GetMapping("/sort/priority/desc")
    public String orderByPriorityDesc(Model model) {
        List<Task> tasks = service.orderByPriorityDesc();

        model.addAttribute("tasks", service.getTaskAttributesDTO(tasks));

        return "tasks";
    }

    @GetMapping("/sort/state/asc")
    public String orderByStateAsc(Model model) {
        List<Task> tasks = service.orderByStateAsc();

        model.addAttribute("tasks", service.getTaskAttributesDTO(tasks));

        return "tasks";
    }

    @GetMapping("/sort/state/desc")
    public String orderByStateDesc(Model model) {
        List<Task> tasks = service.orderByStateDesc();

        model.addAttribute("tasks", service.getTaskAttributesDTO(tasks));

        return "tasks";
    }

    @GetMapping("/sort/created/asc")
    public String orderByCreatedAsc(Model model) {
        List<Task> tasks = service.orderByDateOfCreationAsc();

        model.addAttribute("tasks", service.getTaskAttributesDTO(tasks));

        return "tasks";
    }

    @GetMapping("/sort/created/desc")
    public String orderByCreatedDesc(Model model) {
        List<Task> tasks = service.orderByDateOfCreationDesc();

        model.addAttribute("tasks", service.getTaskAttributesDTO(tasks));

        return "tasks";
    }
}
