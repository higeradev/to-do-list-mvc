package taskmanager.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import taskmanager.DTO.TaskDTO;
import taskmanager.services.TaskService;

import java.util.Map;

@Controller
public class CreateTaskController {
    private final TaskService service;

    public CreateTaskController(TaskService service) {
        this.service = service;
    }

    @PostMapping("/add/task")
    public String create(@RequestParam Map<String, String> params) {

        service.create(new TaskDTO(params.get("title"), params.get("priority"), params.get("description")));

        return "redirect:/";
    }
}
