package taskmanager.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import taskmanager.services.TaskService;

@Controller
public class CloseTaskController {
    private final TaskService service;

    public CloseTaskController(TaskService service) {
        this.service = service;
    }

    @PostMapping("/close/task/{id}")
    public String close(@PathVariable String id) {
        service.closeTask(id);

        return "redirect:/";
    }
}
