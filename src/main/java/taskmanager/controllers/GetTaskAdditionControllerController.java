package taskmanager.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import taskmanager.models.Priority;
import taskmanager.services.TaskService;
import taskmanager.utils.Generator;

import java.util.List;

@Controller
public class GetTaskAdditionControllerController {
    private final TaskService service;

    public GetTaskAdditionControllerController(TaskService service) {
        this.service = service;
    }

    @GetMapping("/add/task")
    public String get(Model model) {
        List<Priority> priorities = Generator.generatePriorityList();
        model.addAttribute("priorities", priorities);

        return "add-task";
    }
}
