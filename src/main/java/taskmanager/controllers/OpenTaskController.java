package taskmanager.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import taskmanager.services.TaskService;

@Controller
public class OpenTaskController {
    private final TaskService service;

    public OpenTaskController(TaskService service) {
        this.service = service;
    }

    @PostMapping("/open/task/{id}")
    public String open(@PathVariable String id) {
        service.openTask(id);

        return "redirect:/";
    }
}
