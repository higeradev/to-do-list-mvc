package taskmanager.services;

import taskmanager.DTO.TaskAttributesDTO;
import taskmanager.DTO.TaskDTO;
import taskmanager.models.Task;

import java.util.List;

public interface TaskService {
    void create(TaskDTO taskDTO);
    void save(Task task);
    void delete(String taskId);
    List<Task> findAll();
    Task findById(String taskId);
    void openTask(String taskId);
    void closeTask(String taskId);

    TaskAttributesDTO getTaskAttributesDTO(List<Task> tasks);
    List<Task> orderByTitleAsc();
    List<Task> orderByTitleDesc();
    List<Task> orderByPriorityAsc();
    List<Task> orderByPriorityDesc();
    List<Task> orderByStateAsc();
    List<Task> orderByStateDesc();
    List<Task> orderByDateOfCreationAsc();
    List<Task> orderByDateOfCreationDesc();
    List<Task> findByPriority(String priority);
    List<Task> findByStateName(String stateName);
    List<Task> filterByWordInTitle(String word);
    List<Task> filterByWordInDescription(String word);
    List<Task> filterByState(String state);
    List<Task> filterByPriority(String priority);
}
