package taskmanager.services.impl;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import taskmanager.DTO.TaskAttributesDTO;
import taskmanager.DTO.TaskDTO;
import taskmanager.models.Task;
import taskmanager.repositories.TaskRepository;
import taskmanager.services.TaskService;
import taskmanager.utils.Generator;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class TaskServiceImpl implements TaskService {
    private final TaskRepository repository;

    public TaskServiceImpl(TaskRepository repository) {
        this.repository = repository;
    }

    @Override
    public void create(TaskDTO taskDTO) {
        Task task = Task
                .builder()
                                .title(taskDTO.getTitle())
                                .priority(taskDTO.getPriority())
                                .description(taskDTO.getDescription())
                                .build();
        task.setId();
        task.setDateOfCreation();
        repository.save(task);
    }

    @Override
    public void save(Task task) {
        repository.save(task);
    }

    @Override
    public void delete(String taskId) {
        repository.deleteById(taskId);
    }

    @Override
    public List<Task> findAll() {
        return repository.getAll();
    }

    @Override
    public Task findById(String taskId) {
        Optional<Task> task = repository.findById(taskId);

        if (task.isEmpty()) {
            throw new NoSuchElementException("Task with ID %s doesn't exist".formatted(taskId));
        }

        return task.get();
    }

    @Override
    public void openTask(String taskId) {
        Task task = findById(taskId);
        task.openTask();
        task.setDateOfOpening();

        repository.save(task);
    }

    @Override
    public void closeTask(String taskId) {
        Task task = findById(taskId);
        task.closeTask();
        task.setDateOfClosing();

        repository.save(task);
    }

    @Override
    public TaskAttributesDTO getTaskAttributesDTO(List<Task> tasks) {
        return TaskAttributesDTO
                                .builder()
                                .tasks(tasks)
                                .priorities(Generator.generatePriorityList())
                                .states(Generator.generateStateList())
                                .build();
    }

    @Override
    public List<Task> orderByTitleAsc() {
        List<Task> tasks = repository.getAll();

        return tasks
                    .stream()
                    .sorted(Comparator
                    .comparing(Task::getTitle, String.CASE_INSENSITIVE_ORDER))
                    .collect(Collectors.toList());
    }

    @Override
    public List<Task> orderByTitleDesc() {
        List<Task> tasks = repository.getAll();

        return tasks
                    .stream()
                    .sorted(Comparator.comparing(Task::getTitle, String.CASE_INSENSITIVE_ORDER).reversed())
                    .collect(Collectors.toList());
    }

    @Override
    public List<Task> orderByPriorityAsc() {
        Sort.Order order = new Sort.Order(Sort.Direction.ASC, "priority");

        return repository.findAll(Sort.by(order));
    }

    @Override
    public List<Task> orderByPriorityDesc() {
        Sort.Order order = new Sort.Order(Sort.Direction.DESC, "priority");

        return repository.findAll(Sort.by(order));
    }

    @Override
    public List<Task> orderByStateAsc() {
        Sort.Order order = new Sort.Order(Sort.Direction.ASC, "stateName");

        return repository.findAll(Sort.by(order));
    }

    @Override
    public List<Task> orderByStateDesc() {
        Sort.Order order = new Sort.Order(Sort.Direction.DESC, "stateName");

        return repository.findAll(Sort.by(order));
    }

    @Override
    public List<Task> orderByDateOfCreationAsc() {
        Sort.Order order = new Sort.Order(Sort.Direction.ASC, "dateOfCreation");

        return repository.findAll(Sort.by(order));
    }

    @Override
    public List<Task> orderByDateOfCreationDesc() {
        Sort.Order order = new Sort.Order(Sort.Direction.DESC, "dateOfCreation");

        return repository.findAll(Sort.by(order));
    }

    @Override
    public List<Task> findByPriority(String priority) {
        return repository.findByPriority(priority);
    }

    @Override
    public List<Task> findByStateName(String stateName) {
        return repository.findByStateName(stateName);
    }

    @Override
    public List<Task> filterByWordInTitle(String word) {
        List<Task> tasks = repository.getAll();
        return tasks.stream().filter(it -> it.getTitle().contains(word)).collect(Collectors.toList());
    }

    @Override
    public List<Task> filterByWordInDescription(String word) {
        List<Task> tasks = repository.getAll();
        return tasks.stream().filter(it -> it.getDescription().contains(word)).collect(Collectors.toList());
    }

    @Override
    public List<Task> filterByState(String state) {
        return repository.findByStateName(state);
    }

    @Override
    public List<Task> filterByPriority(String priority) {
        return repository.findByPriority(priority);
    }


}
