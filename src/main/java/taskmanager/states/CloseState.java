package taskmanager.states;

import taskmanager.models.Task;

public class CloseState implements State{
    @Override
    public void open(Task task) {

    }

    @Override
    public void close(Task task) {

    }
}
