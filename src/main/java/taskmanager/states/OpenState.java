package taskmanager.states;

import taskmanager.models.Task;

public class OpenState implements State{
    @Override
    public void open(Task task) {

    }

    @Override
    public void close(Task task) {
        task.setState(new CloseState());
    }
}
