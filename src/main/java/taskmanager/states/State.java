package taskmanager.states;

import taskmanager.models.Task;

public interface State {
    void open(Task task);
    void close(Task task);
}
