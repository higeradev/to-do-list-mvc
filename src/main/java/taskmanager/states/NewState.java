package taskmanager.states;

import taskmanager.models.Task;

public class NewState implements State{
    @Override
    public void open(Task task) {
        task.setState(new OpenState());
    }

    @Override
    public void close(Task task) {
    }
}
