package taskmanager.utils;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import taskmanager.models.Task;
import taskmanager.repositories.TaskRepository;
import taskmanager.states.NewState;
import taskmanager.states.OpenState;

import java.util.UUID;
import java.util.stream.Stream;

@Configuration
public class DatabasePreloader {
    @Bean
    CommandLineRunner initDatabase(TaskRepository repository) {
        repository.deleteAll();

        return (args) -> Stream.of(tasks())
                .peek(System.out::println)
                .forEach(repository::save);
    }

    private Task[] tasks() {
        return new Task[] {
                new Task(
                        UUID.randomUUID().toString(),
                        "Do homework",
                        "High",
                        new NewState(),
                        "New",
                        "Description of new task. Please don't forget commits!",
                        "03.05.2023, 19:13",
                        null,
                        null),
                new Task(
                        UUID.randomUUID().toString(),
                        "Workout",
                        "Middle",
                        new OpenState(),
                        "Open",
                        "Go to the gym",
                        Generator.getCurrentDate(),
                        null,
                        null),
        };
    }
}
