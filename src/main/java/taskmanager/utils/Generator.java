package taskmanager.utils;

import taskmanager.models.Priority;
import taskmanager.models.States;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class Generator {

    public static String getCurrentDate() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy, HH:mm");
        return now.format(formatter);
    }

    public static List<States> generateStateList() {
        return new ArrayList<>(EnumSet.allOf(States.class));
    }

    public static List<Priority> generatePriorityList() {
        return new ArrayList<>(EnumSet.allOf(Priority.class));
    }
}
