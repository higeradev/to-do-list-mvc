package taskmanager.DTO;

import lombok.*;
import taskmanager.models.Priority;
import taskmanager.models.States;
import taskmanager.models.Task;

import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskAttributesDTO {
    private List<Task> tasks;
    private List<Priority> priorities;
    private List<States> states;
}
