package taskmanager.repositories;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import taskmanager.models.Task;

import java.util.List;

@Repository
public interface TaskRepository extends CrudRepository<Task, String> {
    @Query("{}")
    List<Task> getAll();
    List<Task> findAll(Sort sort);
    List<Task> findByPriority(String priority);
    List<Task> findByStateName(String stateName);
}
