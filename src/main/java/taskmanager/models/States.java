package taskmanager.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum States {
    NEW("New"),
    OPEN("Open"),
    CLOSED("Closed");

    private final String value;
}
