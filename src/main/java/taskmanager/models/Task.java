package taskmanager.models;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import taskmanager.states.NewState;
import taskmanager.states.State;
import taskmanager.utils.Generator;

import java.util.UUID;

@Builder
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "tasks")
public class Task {
    @Id
    private String id;

    private String title;

    private String priority;
    private State state = new NewState();
    private String stateName = getStateName();
    private String description;
    private String dateOfCreation;
    private String dateOfOpening;
    private String dateOfClosing;

    public void setId() {
        this.id = UUID.randomUUID().toString();
    }

    public void openTask() {
        state.open(this);
        this.stateName = getStateName();
    }

    public void closeTask() {
        state.close(this);
        this.stateName = getStateName();
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public void setDateOfCreation() {
        this.dateOfCreation = Generator.getCurrentDate();
    }

    public void setDateOfClosing() {
        this.dateOfClosing = Generator.getCurrentDate();
    }

    public void setDateOfOpening() {
        this.dateOfOpening = Generator.getCurrentDate();
    }

    public String getStateName() {
        String stateName = null;
        switch (state.getClass().getSimpleName()) {
            case "NewState" -> stateName = "New";
            case "OpenState" -> stateName = "Open";
            case "CloseState" -> stateName = "Closed";
        }
        return stateName;
    }
}
