package taskmanager.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Priority {
    HIGH("High"),
    LOW("Low"),
    MIDDLE("Middle");

    private final String value;

}
